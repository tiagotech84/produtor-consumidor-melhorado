package br.ucsal.ads.so2020;

public class Produtor extends Thread {
	
	private Armazem armazem;
	
	public Produtor (Armazem armazem) {
		this.armazem = armazem;
	}
	
	public void run(){
		while(true) {
			armazem.depositar();
			try {
				int espera = (int) (Math.random() * 1000);
				sleep(espera);
			} catch(InterruptedException ie) {
				System.out.println("Interrompido!");
			}
		}
	}

}
