package br.ucsal.ads.so2020;

public class Consumidor extends Thread {

	private Armazem armazem;

	public Consumidor(Armazem armazem) {
		this.armazem = armazem;
	}

	public void run() {
		while(true) {
			armazem.pegar();
			try {
				int espera = (int) (Math.random() * 1000);
				sleep(espera);
			} catch(InterruptedException ie) {
				System.out.println("Interrompido!");
			}
		}
	}
}

