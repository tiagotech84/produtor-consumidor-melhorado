package br.ucsal.ads.so2020;

public class Armazem {

	private boolean[] estoque = new boolean[5];

	private int primeiroProdutoDisponivel = 0;
	private int primeiroEspacoVazio = 0;

	public Armazem() {
		for(int i = 0; i < estoque.length; i++) {
			estoque[i] = false;
		}
	}

	public synchronized void pegar() {
		while(estaVazio(primeiroProdutoDisponivel)) {
			try {
				wait();
			} catch(InterruptedException ie) {
				System.out.println("Interrompido");
			}
		}

		estoque[primeiroProdutoDisponivel] = false;
		notifyAll();
		System.out.printf("Produto retirado!\t Total disponível: %d produtos\n", quantidadeDisponivel());

		if (primeiroProdutoDisponivel == (estoque.length - 1)) {
			primeiroProdutoDisponivel = 0;
		} else {
			primeiroProdutoDisponivel++;
		}
	}

	public synchronized void depositar() {
		while(!estaVazio(primeiroEspacoVazio)) {
			try {
				wait();
			} catch(InterruptedException ie) {
				System.out.println("Interrompido");
			}
		}

		estoque[primeiroEspacoVazio] = true;
		notifyAll();
		System.out.printf("Produto depositado!\t Total disponível: %d produtos\n", quantidadeDisponivel());

		if (primeiroEspacoVazio == (estoque.length - 1)) {
			primeiroEspacoVazio = 0;
		} else {
			primeiroEspacoVazio++;
		}
	}

	private boolean estaVazio(int pos) {
		return !estoque[pos];
	}

	private int quantidadeDisponivel() {
		int count = 0;
		for(boolean produto: estoque){
			count += produto ? 1 : 0;
		}
		return count;
	}


}
