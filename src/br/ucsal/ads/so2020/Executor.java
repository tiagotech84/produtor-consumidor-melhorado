package br.ucsal.ads.so2020;

public class Executor {
	
	public static void main(String[] args) throws InterruptedException {
		Armazem a = new Armazem();
		
		Produtor p1 = new Produtor(a);
		Consumidor c1 = new Consumidor(a);
		
		p1.setName("Thread Produtor 1");
		p1.start();

		c1.setName("Thread Consumidor 1");
		c1.start();
	}

}
